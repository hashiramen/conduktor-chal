/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Cluster = {
    id: string;
    name: string;
}