/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Cluster } from '../models/Cluster';
import type { Topic } from '../models/Topic';
import { request as __request } from '../core/request';

export class Service {
  /**
   * @returns Cluster List the clusters available
   * @returns any
   * @throws ApiError
   */
  public static async listClusters(): Promise<Array<Cluster> | any> {
    const result = await __request({
      method: 'GET',
      path: `/clusters`,
    });
    return result.body;
  }

  /**
   * @param requestBody Friendly name of the cluster to create
   * @returns any
   * @throws ApiError
   */
  public static async createCluster(
    requestBody: string
  ): Promise<any> {
    const result = await __request({
      method: 'POST',
      path: `/clusters`,
      body: requestBody,
    });
    return result.body;
  }

  /**
   * @param id ID of the cluster to delete
   * @returns any
   * @throws ApiError
   */
  public static async deleteCluster(id: string): Promise<any> {
    const result = await __request({
      method: 'DELETE',
      path: `/clusters/${id}`,
    });
    return result.body;
  }

  /**
   * @param id
   * @returns Topic List of the topics for this cluster
   * @returns any
   * @throws ApiError
   */
  public static async listTopics(
    id: string
  ): Promise<Array<Topic> | any> {
    const result = await __request({
      method: 'GET',
      path: `/clusters/${id}/topics`,
    });
    return result.body;
  }

  /**
   * @param id Cluster ID
   * @param requestBody Name of the topic to create
   * @returns any
   * @throws ApiError
   */
  public static async createTopic(
    id: string,
    requestBody: string
  ): Promise<any> {
    const result = await __request({
      method: 'POST',
      path: `/clusters/${id}/topics`,
      body: requestBody,
    });
    return result.body;
  }

  /**
   * !!Important note - Since I wanted to keep it simple and just make the use of the mock api
   * I have removed the "/data" after the topic id, since mock api did not had that flexibility and I had no time to spin up my own API server
   * @param id Cluster ID
   * @param topic Name of the topic to fetch data from
   * @returns any Data from the topic
   * @throws ApiError
   */
  public static async getData(
    id: string,
    topic: string
  ): Promise<any> {
    const result = await __request({
      method: 'GET',
      path: `/clusters/${id}/topics/${topic}`,
    });
    return result.body;
  }
}
