import * as React from 'react';

export enum Language {
  en = 'en',
  kr = 'kr',
}

const LocaleContext = React.createContext<Record<string, string>>(
  require('./en.json')
);

export default LocaleContext;

export const useLocale = () => {
  const [locale, setLocale] = React.useState<string>(
    localStorage.getItem('locale') ?? Language.en
  );

  React.useEffect(() => {
    localStorage.setItem('locale', locale);
  }, [locale]);

  const translation = require(`./${locale}.json`);

  const handleSetLocale = (locale: keyof typeof Language) =>
    void setLocale(locale);

  return {
    translation,
    locale,
    handleSetLocale,
  };
};
