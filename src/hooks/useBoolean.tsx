import * as React from 'react';

export const useBoolean = () => {
  const [valid, setValid] = React.useState<boolean>(false);

  return { valid, setValid };
};
