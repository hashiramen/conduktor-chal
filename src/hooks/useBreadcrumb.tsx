import * as React from 'react';

export const useBreadcrumb = (pathname: string, baseUrl?: string) => {
  const pathnameSplitted = React.useMemo(() => {
    return pathname.split('/');
  }, [pathname]);

  const defineUrl = React.useCallback(
    (index: number) => {
      return index + 1 === pathnameSplitted.length
        ? undefined
        : pathnameSplitted
            .slice(0, index + 1)
            .reduce(
              (cur, next) => `${cur}${next}/`,
              baseUrl ?? window.location.origin
            );
    },
    [pathnameSplitted, baseUrl]
  );

  return {
    pathnameSplitted,
    defineUrl,
  };
};
