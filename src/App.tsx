import * as React from 'react';

interface IAppProps {}

const App: React.FunctionComponent<IAppProps> = () => {
  return <p>app</p>;
};

export default App;
