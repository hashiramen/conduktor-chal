import * as React from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom';
import MainLayout from './Layouts/MainLayout';
import AuthPage from './Pages/Auth/AuthPage';
import ClustersPage from './Pages/Clusters/ClustersPage';
import LandingPage from './Pages/LandingPage';
import ProtectedRoute from './ProtectedRoute';

const Routes: React.FunctionComponent<unknown> = () => {
  return (
    <BrowserRouter>
      <Switch>
        <ProtectedRoute
          exact
          path="/"
          render={(props) => (
            <MainLayout {...props}>
              <LandingPage {...props} />
            </MainLayout>
          )}
        />
        <ProtectedRoute
          path="/clusters"
          render={(props) => (
            <MainLayout {...props}>
              <ClustersPage {...props} />
            </MainLayout>
          )}
        />
        <Route path="/auth" component={AuthPage} />
        <Route render={() => <span>not found</span>} />
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
