import { Button, List } from 'antd';
import * as React from 'react';
import LocaleContext from '../../context/LocaleContext';
import { useBoolean } from '../../hooks/useBoolean';
import { Service, Topic } from '../../services/openapi';
import ListItem from '../Molecules/ListItem';

interface ITopicsListProps {
  clusterId: string;
}

const TopicsList: React.FunctionComponent<ITopicsListProps> = ({
  clusterId,
}) => {
  const [topics, setTopics] = React.useState<Topic[]>([]);

  const { valid, setValid } = useBoolean();

  React.useEffect(() => {
    const browse = async () => {
      setValid(true);
      const response: Topic[] = await Service.listTopics(clusterId);

      setTopics(response);
      setValid(false);
    };

    browse();
  }, [clusterId, setValid]);

  const handleCreate = () => {
    const create = async () => {
      setValid(true);
      const response: Topic = await Service.createTopic(
        clusterId,
        ''
      );
      setTopics([response, ...topics]);
      setValid(false);
    };

    create();
  };
  return (
    <LocaleContext.Consumer>
      {(translation) => (
        <div>
          <Button
            onClick={handleCreate}
            style={{
              position: 'sticky',
              top: 1,
              left: 0,
              width: '100%',
              zIndex: 1,
            }}
          >
            {translation['createTopic']}
          </Button>
          <List
            itemLayout="horizontal"
            loading={valid}
            dataSource={topics}
            renderItem={(item) => (
              <ListItem
                urlPath={`/clusters/${clusterId}/topics/${item.name}`}
                title={item.name}
                description={translation['topicName']}
              />
            )}
          />
        </div>
      )}
    </LocaleContext.Consumer>
  );
};

export default TopicsList;
