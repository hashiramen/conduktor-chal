import { Button, List } from 'antd';
import * as React from 'react';
import LocaleContext from '../../context/LocaleContext';
import { useBoolean } from '../../hooks/useBoolean';
import { Cluster, Service } from '../../services/openapi';
import ListItem from '../Molecules/ListItem';

interface IClusterListProps {}

const ClusterList: React.FunctionComponent<IClusterListProps> =
  () => {
    const [clusters, setClusters] = React.useState<Cluster[]>([]);

    const { valid, setValid } = useBoolean();

    React.useEffect(() => {
      const browse = async () => {
        setValid(true);
        const response: Cluster[] = await Service.listClusters();

        setClusters(response);
        setValid(false);
      };

      browse();
    }, [setValid]);

    const handleCreate = () => {
      const create = async () => {
        setValid(true);
        const response: Cluster = await Service.createCluster('');
        setClusters([response, ...clusters]);
        setValid(false);
      };

      create();
    };

    return (
      <LocaleContext.Consumer>
        {(translation) => (
          <div>
            <Button
              onClick={handleCreate}
              style={{
                position: 'sticky',
                top: 1,
                left: 0,
                width: '100%',
                zIndex: 1,
              }}
            >
              {translation['createCluster']}
            </Button>
            <List
              itemLayout="horizontal"
              loading={valid}
              dataSource={clusters}
              renderItem={(item) => (
                <ListItem
                  urlPath={`/clusters/${item.id}`}
                  title={item.name}
                  description={translation['clusterName']}
                />
              )}
            />
          </div>
        )}
      </LocaleContext.Consumer>
    );
  };

export default ClusterList;
