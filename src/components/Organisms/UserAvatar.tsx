import {
  Avatar,
  Button,
  Row,
  Space,
  Tooltip,
  Typography,
} from 'antd';
import { LogoutOutlined, UserOutlined } from '@ant-design/icons';
import * as React from 'react';
import { useAuth0 } from '@auth0/auth0-react';

interface IUserAvatarProps {}

const UserAvatar: React.FunctionComponent<IUserAvatarProps> = () => {
  const { user, isAuthenticated, logout } = useAuth0();

  return (
    <div>
      <Row justify="end" align="middle">
        {isAuthenticated && user ? (
          <Space>
            <Typography.Text strong style={{ color: 'white' }}>
              {user.name}
            </Typography.Text>
            <Avatar
              shape="square"
              icon={<UserOutlined />}
              src={user.picture}
            />
            <Tooltip title="Sign out">
              <Button
                type="primary"
                shape="circle"
                icon={<LogoutOutlined />}
                onClick={() =>
                  void logout({
                    returnTo: window.location.origin,
                  })
                }
              />
            </Tooltip>
          </Space>
        ) : (
          <span>Loading user info...</span>
        )}
      </Row>
    </div>
  );
};

export default UserAvatar;
