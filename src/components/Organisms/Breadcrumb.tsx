import { HomeOutlined } from '@ant-design/icons';
import * as React from 'react';
import { Breadcrumb as AntBreadcrumb } from 'antd';
import { useLocation } from 'react-router';
import { useBreadcrumb } from '../../hooks/useBreadcrumb';

interface IBreadcrumbProps {}

const Breadcrumb: React.FunctionComponent<IBreadcrumbProps> = () => {
  const location = useLocation();

  const { defineUrl, pathnameSplitted } = useBreadcrumb(
    location.pathname
  );

  return (
    <AntBreadcrumb>
      {pathnameSplitted.map((name, index) =>
        !name && !index ? (
          <AntBreadcrumb.Item key={index} href="/">
            <HomeOutlined style={{ color: 'white' }} />
          </AntBreadcrumb.Item>
        ) : (
          <AntBreadcrumb.Item key={index} href={defineUrl(index)}>
            <span style={{ color: 'white' }}>{name}</span>
          </AntBreadcrumb.Item>
        )
      )}
    </AntBreadcrumb>
  );
};

export default Breadcrumb;
