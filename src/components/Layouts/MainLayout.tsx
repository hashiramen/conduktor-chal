import { Row, Layout, Col, Button } from 'antd';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import UserAvatar from '../Organisms/UserAvatar';
import Breadcrumb from '../Organisms/Breadcrumb';
import { blue } from '@ant-design/colors';
import LocaleContext, {
  Language,
  useLocale,
} from '../../context/LocaleContext';

const { Header, Content } = Layout;

interface IMainLayoutProps extends RouteComponentProps {}

const MainLayout: React.FunctionComponent<IMainLayoutProps> = ({
  children,
}) => {
  const { translation, locale, handleSetLocale } = useLocale();
  return (
    <LocaleContext.Provider value={translation}>
      <Row
        justify="center"
        align="top"
        style={{
          height: '100%',
        }}
      >
        <Layout
          style={{
            height: 'inherit',
            maxWidth: '1080px',
          }}
        >
          <Layout>
            <Header
              style={{
                backgroundColor: blue.primary,
              }}
            >
              <Row
                justify="space-between"
                align="middle"
                style={{
                  height: '100%',
                }}
              >
                <div>
                  <Breadcrumb />
                </div>
                <div>
                  <span>Locale </span>
                  <Button
                    disabled={locale === Language.en}
                    onClick={() => void handleSetLocale('en')}
                  >
                    {Language.en}
                  </Button>
                  <Button
                    disabled={locale === Language.kr}
                    onClick={() => void handleSetLocale('kr')}
                  >
                    {Language.kr}
                  </Button>
                </div>
                <div>
                  <UserAvatar />
                </div>
              </Row>
            </Header>
            <Content>
              <Row
                style={{
                  maxHeight: '100%',
                  overflowY: 'auto',
                  position: 'relative',
                }}
              >
                <Col flex="auto">{children}</Col>
              </Row>
            </Content>
          </Layout>
        </Layout>
      </Row>
    </LocaleContext.Provider>
  );
};

export default MainLayout;
