import { Col, Row, Spin } from 'antd';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import LocaleContext from '../../../context/LocaleContext';
import { useBoolean } from '../../../hooks/useBoolean';
import { Service, Topic } from '../../../services/openapi';

interface IDetailsPageProps
  extends RouteComponentProps<{ id: string; topicName: string }> {}

const DetailsPage: React.FunctionComponent<IDetailsPageProps> = ({
  match,
}) => {
  const { valid, setValid } = useBoolean();

  const [topic, setTopic] = React.useState<Topic | null>(null);

  React.useEffect(() => {
    const get = async () => {
      setValid(true);
      const response: Topic = await Service.getData(
        match.params.id,
        match.params.topicName
      );
      setValid(false);

      setTopic(response);
    };

    get();
  }, [match.params.id, match.params.topicName, setValid]);
  return (
    <LocaleContext.Consumer>
      {(translation) => (
        <Row style={{ padding: '3rem' }}>
          {valid ? (
            <Spin />
          ) : (
            <React.Fragment>
              <Col span={24}>
                <h1>{translation['topicOverview']}</h1>
              </Col>
              <Col span={24}>
                <span>
                  <span style={{ opacity: 0.85 }}>Name - </span>{' '}
                  {topic?.name}
                </span>
              </Col>
            </React.Fragment>
          )}
        </Row>
      )}
    </LocaleContext.Consumer>
  );
};

export default DetailsPage;
