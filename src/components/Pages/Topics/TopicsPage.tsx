import * as React from 'react';
import {
  Redirect,
  Route,
  RouteComponentProps,
  Switch,
} from 'react-router-dom';
import DetailsPage from './DetailsPage';

interface ITopicsPageProps extends RouteComponentProps {}

const TopicsPage: React.FunctionComponent<ITopicsPageProps> = ({
  match,
}) => {
  return (
    <Switch>
      <Route
        exact
        path={match.path}
        render={() => <Redirect to="/clusters" />}
      />
      <Route
        path={`${match.path}/:topicName`}
        component={DetailsPage}
      />
    </Switch>
  );
};

export default TopicsPage;
