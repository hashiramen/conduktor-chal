import * as React from 'react';
import { Redirect, RouteComponentProps } from 'react-router-dom';

interface ILandingPageProps extends RouteComponentProps {}

const LandingPage: React.FunctionComponent<ILandingPageProps> =
  () => {
    return <Redirect to="/clusters" />;
  };

export default LandingPage;
