import * as React from 'react';
import ClusterList from '../../Organisms/ClusterList';

interface IListProps {}

const List: React.FunctionComponent<IListProps> = () => {
  return <ClusterList />;
};

export default List;
