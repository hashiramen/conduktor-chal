import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import TopicsPage from '../Topics/TopicsPage';
import DetailsPage from './DetailsPage';
import ListPage from './ListPage';

interface IClustersPageProps extends RouteComponentProps {}

const ClustersPage: React.FunctionComponent<IClustersPageProps> = ({
  match,
}) => {
  return (
    <Switch>
      <Route exact path={match.path} component={ListPage} />
      <Route
        path={`${match.path}/:id/topics`}
        component={TopicsPage}
      />
      <Route path={`${match.path}/:id`} component={DetailsPage} />
    </Switch>
  );
};

export default ClustersPage;
