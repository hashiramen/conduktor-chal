import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import TopicsList from '../../Organisms/TopicsList';

interface IDetailsPageProps
  extends RouteComponentProps<{
    id: string;
  }> {}

const DetailsPage: React.FunctionComponent<IDetailsPageProps> = ({
  match,
}) => {
  return <TopicsList clusterId={match.params.id} />;
};

export default DetailsPage;
