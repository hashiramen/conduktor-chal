import { useAuth0 } from '@auth0/auth0-react';
import * as React from 'react';
import { RouteComponentProps, Redirect } from 'react-router-dom';

interface IAuthorizePageProps extends RouteComponentProps {}

const AuthorizePage: React.FunctionComponent<IAuthorizePageProps> =
  () => {
    const { isAuthenticated } = useAuth0();

    return !isAuthenticated ? (
      <span>Authorizing</span>
    ) : (
      <Redirect to="/" />
    );
  };

export default AuthorizePage;
