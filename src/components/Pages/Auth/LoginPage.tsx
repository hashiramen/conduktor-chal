import { useAuth0 } from '@auth0/auth0-react';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

interface ILoginPageProps extends RouteComponentProps {}

const LoginPage: React.FunctionComponent<ILoginPageProps> = () => {
  const { loginWithRedirect } = useAuth0();
  React.useEffect(() => {
    loginWithRedirect();
  }, [loginWithRedirect]);
  return null;
};

export default LoginPage;
