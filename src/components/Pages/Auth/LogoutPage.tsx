import { useAuth0 } from '@auth0/auth0-react';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

interface ILogoutPageProps extends RouteComponentProps {}

const LogoutPage: React.FunctionComponent<ILogoutPageProps> = () => {
  const { logout } = useAuth0();
  React.useEffect(() => {
    logout();
  }, [logout]);
  return null;
};

export default LogoutPage;
