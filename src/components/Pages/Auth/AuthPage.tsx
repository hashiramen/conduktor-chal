import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import AuthorizePage from './AuthorizePage';
import LoginPage from './LoginPage';
import LogoutPage from './LogoutPage';

interface IAuthPageProps extends RouteComponentProps {}

const AuthPage: React.FunctionComponent<IAuthPageProps> = ({
  match,
}) => {
  return (
    <Switch>
      <Route path={`${match.path}/login`} component={LoginPage} />
      <Route path={`${match.path}/logout`} component={LogoutPage} />
      <Route
        path={`${match.path}/authorize`}
        component={AuthorizePage}
      />
    </Switch>
  );
};

export default AuthPage;
