import { useAuth0 } from '@auth0/auth0-react';
import * as React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';

interface IProtectedRouteProps extends RouteProps {}

const ProtectedRoute: React.FunctionComponent<IProtectedRouteProps> =
  ({ component: Component, render, ...rest }) => {
    const { isAuthenticated, isLoading } = useAuth0();

    const N: any = typeof render === 'function' ? render : Component;

    return (
      <Route
        {...rest}
        render={(props) =>
          !isAuthenticated && !isLoading ? (
            <Redirect to="/auth/login" />
          ) : (
            <N {...props} />
          )
        }
      />
    );
  };

export default ProtectedRoute;
