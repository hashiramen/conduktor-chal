import { List } from 'antd';
import * as React from 'react';
import { Link } from 'react-router-dom';

interface IListItemProps {
  urlPath: string;
  title: string;
  description: string;
}

const ListItem: React.FunctionComponent<IListItemProps> = ({
  title,
  urlPath,
  description,
}) => {
  return (
    <List.Item
      style={{
        padding: '3rem',
        margin: 0,
        backgroundColor: '#efefef',
        boxShadow: '0 0 1px rgba(34, 25, 25, 0.4)',
      }}
    >
      <List.Item.Meta
        title={
          <Link
            to={urlPath}
            style={{
              fontSize: '1.4rem',
            }}
          >
            {title}
          </Link>
        }
        description={description}
      />
    </List.Item>
  );
};

export default ListItem;
