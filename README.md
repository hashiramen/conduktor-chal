### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


### 'env variables'

Normally I would not keep them in the repo but for the purpose of the challenge
there is a 'env.development.local' file with already set env variables

## Things that I did

* Generated an API client service and its schema based on the openapi.yaml
* Used a https://mockapi.io/ for that API mocking server purposes and generated a schema that reflac openapi structure
* Used react-router-dom to add some basic routes to the project. Normally I would not hard code routes like that but for the "MVP" challenge purposed
that does its job
* There is a protected route which check for if we are logged in and if not its redirects us to auth page
* Added a super simple react context with locales english and korean. I would always recommend using i18n instead thought.
* Implemented auth0 flow. Redirects the user on entrance to the login page where they can either sign up or login.
Also added an option to logout
* Displaying the avatar and the logged in user nickname
* Added breadcrumb - mostly only for the mvp purpose - so that we can see what is going on, since the project is barebone
* Listing of clusters / Listing of topics and loadin the data of the topic with the use of https://mockapi.io/
The listing is also pretty much barebone but that is to keep thins simple. That is why the design is also not crazy at all
* Used antdesign, to demnostrate that I can move around with it. I did used inline "styles" that normally I would rather stay away from but
I did not have more itme to play around with it.
* Create Cluster / Create Topic - it creates a instance of cluster or topic with the https://mockapi.io/ - 
I did not included a custom names because appereantly mockapi just generates them randomly but the client api itself does supports that
* I created my custom auth0 to reidrect my to specific authorize pages, was just more convinent at that point